'''
/* Copyright 2018 Sean Behan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Written by: Sean Behan
 * Description: Page replacement simulator
 * Usage: python main.py
 * Assumptions:
 *      - using Python 3
 *      - installed all the required libraries
 *      - csv files are in the same directory as this is being run from
 */ if you want to fix this formatting... be my guest
 '''

import csv
import random
# part of the standard library


swap_size = 15
physical_size = 10
# this can be changed

def there_is_room_in(memory, size_limit):
    if len(memory) < size_limit:
        return True
    return False


def least_recently_accessed(timestamps, memory_physical):
    physical_pages = [job[1] for job in memory_physical]
    page = [ts for ts in timestamps if ts in physical_pages][0]
    index = physical_pages.index(page)
    return index


def try_swap(timestamps, memory_physical, memory_swap, swap_type, job, swap_size):
    success = True
    if there_is_room_in(memory_swap, swap_size):
        if swap_type == 'random':
            index_to_swap = random.randrange(len(memory_physical))
            # choose a random page from memory
        elif swap_type == 'least':
            index_to_swap = least_recently_accessed(timestamps, memory_physical)

        memory_swap.append(memory_physical[index_to_swap])
        # append it to swap

        memory_physical.pop(index_to_swap)
        # remove it from memory

        memory_physical.append(job)
        # move page to memory
        success = True
    else:
        success = False
    return timestamps, memory_physical, memory_swap, success


def do_sim(data, sim_type):
    memory_physical = []
    memory_swap = []
    timestamps = []
    hits = []
    faults = []
    insufficient = []
    
    for job in data:
        num = job[0]
        page = job[1]
        physical_pages = [i[1] for i in memory_physical]
        swap_pages = [i[1] for i in memory_swap]

        if page == -999:
            memory_physical = [i for i in memory_physical if i[0] == num]
            memory_swap = [i for i in memory_swap if i[0] == num]
            # remove the pages from memory
        elif page in physical_pages: 
            # if the job is in physical memory
            timestamps.append(page)
            hits.append(page)
            # page hit
        elif page in swap_pages:
            timestamps, memory_physical, memory_swap, success = try_swap(timestamps, memory_physical, memory_swap, sim_type, job, swap_size)
            if success:
                timestamps.append(page)
                faults.append(page)
                # page fault
            else:
                insufficient.append(page)
            # try to move the job into memory
        # if the page doesn't exist add it to memory
        elif len(memory_physical) < physical_size:
            memory_physical.append(job)
            timestamps.append(page)
            # if there is room in physical memory append it
        elif len(memory_swap) < swap_size:
            timestamps, memory_physical, memory_swap, success = try_swap(timestamps, memory_physical, memory_swap, sim_type, job, swap_size)
            if success:
                timestamps.append(page)
                faults.append(page)
                # page fault
            else:
                insufficient.append(page)
            # try to move the job into memory
        else:
            insufficient.append(page)
            # there isn't enough memory
    return timestamps, memory_physical, memory_swap, hits, faults, insufficient


data = []
for i in ['1', '2', '3', '4', '5', '6']:
    data.append('job_data/job_data_' + str(i) + '.csv')
# generate list of csv files to read

for job_data in data:
    with open(job_data, 'r') as f:
        reader = csv.reader(f)
        mydata = list(reader)
    # parse each csv

    mydata = [[int(i[0]), int(i[1])] for i in mydata]
    # convert to integers instead of strings

    for sim_type in ['random', 'least']:
        print('Doing sim', sim_type, 'with', job_data)
        timestamps, memory_physical, memory_swap, hits, faults, insufficient = do_sim(mydata, sim_type)
        print('hits:', len(hits))
        print('faults:', len(faults))
        print('insufficient:', len(insufficient))
    # do each simulation
